﻿import os
import logging
import configparser

from datetime import datetime
from tools.logmtd import setup_logging
import time
from datetime import date

class Common():

    def __init__(self):
        self.today = datetime.now().strftime('%Y-%m-%d')
        self.config_dir = './config/'
        self.config = self.initconfig('conf')

        ''' 初始化日志 '''
        setup_logging()
        self.error = logging.getLogger('error')
        self.detail = logging.getLogger('detail')

    def initconfig(self, content):
        '''
        初始化config文件
        :return:
        '''

        confdict = dict()
        inifile = os.path.join(self.config_dir + 'config.ini')
        conf = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())  # 生成conf对象
        conf.read(inifile, encoding='utf-8')

        # print(conf.sections())  # 显示所有节名称s
        # print(conf.options('conf'))  # 显示节下面的option名称

        for sect in conf.sections():
            if sect == content:
                for opt in conf.options(sect):
                    confdict[opt] = conf.get(sect, opt)
        return confdict

    def now(self):
        """
        当前时间是字符串：年月日时分秒
        :return:
        """
        format = "%Y-%m-%d %H:%M:%S"
        return datetime.now().strftime(format)

    @staticmethod
    def getcurtime():
        """
        当前时间是字符串：年月日时分秒
        :return:
        """
        format = "%Y-%m-%d %H:%M:%S"
        return datetime.now().strftime(format)

    @staticmethod
    def timediff(starttime, endtime):
        """
        计算时间差
        :param starttime:
        :param endtime:
        :return:
        """
        format = "%Y-%m-%d %H:%M:%S"
        return datetime.strptime(endtime, format) - datetime.strptime(starttime, format)

    def get_date(self):
        """
        获取当前日期
        :return:
        """
        return str(date.today())

    def get_time(self):
        """
        生成年月日时分秒的字符串时间
        :return:
        """
        return time.strftime("%y-%m-%d--%H-%M-%S", time.localtime(time.time()))

    def save_screen_shot(self, driver):
        """
        浏览器截图方法
        :return:
        """
        sub_dir_path = self.create_dir_path("screenShot")
        screnn_name = os.path.join(sub_dir_path, (self.get_time() + "_screenShot.png"))
        sign = driver.get_screenshot_as_file(screnn_name)

        if sign:
            return screnn_name
        else:
            return False


    def get_allure_dir(self):
        """
        将当前生成的report文件夹路径和当前日期以及allure_report字符串
        进行拼接
        :return:
        """
        dir_report_path = self.create_dir_path("report")
        allure_dir = os.path.join(dir_report_path, (self.get_time() + "_allure_report"))
        return allure_dir, self.get_date()

    def get_report_path(self):
        """
        返回report文件夹
        :return:
        """
        dir_report_path = self.create_dir_path("report")
        report_name = os.path.join(dir_report_path, (self.get_time() + "_report.html"))

        return report_name

    def create_dir(self, path):
        """
        确定path文件是否存在，如不存在就创建path文件
        :param path:
        :return:
        """
        if not os.path.exists(path):
            os.makedirs(path)

    def create_dir_path(self, dir):
        """
        将文件名和当前日期拼接成字符串
        :param dir:
        :return:
        """
        root_dir_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), dir)
        sub_dir_path = os.path.join(root_dir_path, self.get_date())

        self.create_dir(sub_dir_path)
        return sub_dir_path
