# -*- coding:utf-8 -*-
"""
 @Time:2021/3/24 9:12
 @Auth: Chh
 @Function:pandas 的公共方法
"""
import pandas as pd
from tools.common import Common

class Pdmethod(Common):

    def __init__(self):
        super().__init__()
        self.filepath = self.config.get('file_path')

        self.openfile()

    def openfile(self,header = 0,sheet_name = 0):
        """
        获取excel文件所有内容
        :param header: header默认为0，如果是1，那么就是略过第一行，从第二行读取
        :param sheet_name: sheet_name默认是0，如果是1，那么就直接取第二个sheet
        :return:
        """
        try:
            self.df = pd.read_excel(self.filepath,header=header,sheet_name=sheet_name)

        except Exception as e:
            self.error.error(e.__str__())

    def gettitle(self):
        """
        获取文件标题
        fillna(value='null').values.tolist()将获取的值包含nan的改为null
        :return:
        """
        self.file_title = self.df.columns.fillna(value='null').values.tolist()

        return self.file_title

    def getinfodata(self):
        """
        获取文件除标题外的所有内容,并将nan转成null
        :return:
        """
        self.file_data = self.df.fillna(value='null').values.tolist()

        return self.file_data

    def getrowdata(self,index=0):
        """
        获取行数据
        :param index: 默认读第一行数据,返回的是行数据的列表
        :return:
        """
        self.rowdata = self.df.loc[index].fillna(value='null').values.tolist()

        return self.rowdata

    def getrow_column(self):
        """
        获取总行数，总列数
        :return:
        """
        self.maxrow = len(self.df.index)
        self.maxcolumn = len(self.df.sum())

        return self.maxrow,self.maxcolumn

    def get_column(self,title = None):
        """
        取出某一列的数据
        :param title:
        :return:
        """
        if title:
            self.column_data = self.df.loc[:,f'{title}']

            return self.column_data
        else:
            return False

    def insert_excel(self,data=None,titles=None,index=None):
        """
        将数据插入到excel表内
        :param data: 字典：[{},{}] 列表：[[],[]]
        :param titles: data是字典时，需要传一组标题
        :param index: 默认是None，插入表内时不插入序号，需要插入需要就将index去掉
        :return:
        """
        if data:

            if isinstance(data[0],dict):
                df = pd.DataFrame(data)
                df.to_excel(self.filepath,index=index)

            elif isinstance(data[0],list):
                df = pd.DataFrame(data,columns=titles)
                df.to_excel(self.filepath,index=None)

        else:
            return False












