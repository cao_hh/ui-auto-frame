# -*- coding:utf-8 -*-
'''auhor: chh'''

import allure

# def argums(issue,feature=None,story=None,title=None,description=None,severity=None):
#     allure.dynamic.feature(feature)
#     allure.dynamic.story(story)
#     allure.dynamic.title(title)
#     allure.dynamic.description(description)
#     allure.dynamic.severity(severity)
#     allure.dynamic.issue(url=issue,name=None)


from pages.wyy.wyy_page import *

import csv
import os
class GetData:

    # logger = Logger()
    co = Common()

    @staticmethod  # 由于各个方法没有关联性，可使用静态方法修饰
    def read_data_to_file(idxbefore=None):
        data = dict()
        filename = GetData().co.config.get('filename')
        if os.path.exists(filename):
            with open(filename, 'r', encoding = 'gb2312') as fr:
                keylist = list(csv.reader(fr))
                for idx,ele in enumerate(keylist):
                    if idxbefore in ele:
                        data[ele[0]] = idx + 1
                    elif f'{idxbefore}_1' in ele:
                        data[ele[0]] = idx

        else:
            raise FileNotFoundError('参数化文件找不到')

        keylist = keylist[data.get(idxbefore):data.get(f'{idxbefore}_1')]
        print(keylist)
        # GetData().logger.info(f'============== {idxbefore}测试用例 入参 ============== :{keylist}')

        return keylist

    @staticmethod
    def read_data_to_excel(file_name):
        pass

    @staticmethod
    def read_data_to_redis(key):
        pass

    @staticmethod
    def read_data_to_mysql(sql):
        pass
