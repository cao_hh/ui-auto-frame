# -*- coding:utf-8 -*-
'''auhor: chh'''


# 重写pytest-emoji
def pytest_emoji_passed(config):
    return u'🍪 ', u'PASSED 🍪 '


def pytest_emoji_failed(config):
    return u'😿 ', u'FAILED 😿 '


def pytest_emoji_skipped(config):
    return u'🙈 ', u'SKIPPED 🙈 '


def pytest_emoji_error(config):
    return u'💩 ', u'ERROR 💩 '


def pytest_emoji_xfailed(config):
    return u'🤓 ', u'xfail 🤓 '


def pytest_emoji_xpassed(config):
    return u'😜 ', u'XPASS 😜 '