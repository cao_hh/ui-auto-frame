#!/usr/bin/env python
# -*- coding:utf-8 -*-  
#====#====#====#====   
#Author:
#CreatDate:
#Version: 
#====#====#====#====
from tools.common import *
from pages.baidu.baidu_page import *
import allure

class NewTestcase():

    logger = Logger()
    co = Common()

    def assertEqual_new(self, first, second,msg=None,driver=None):
        '''
        判断first==second,否则抛错msg
        :param first:
        :param second:
        :param msg:
        :param driver:
        :return:
        '''
        try:
            assert first==second,msg
        except Exception as e:
            self.logger.erro(f"断言失败{str(e)}")
            filename = self.co.save_screen_shot(driver)
            if filename:
                allure.attach.file(filename, '异常截图', allure.attachment_type.PNG)
                raise

    def assertTrue_new(self,exp,msg=None,driver=None):
        '''
        判断exp是否为True
        :param exp:
        :param msg:
        :param driver:
        :return:
        '''
        try:
            assert exp,msg
        except Exception as e:
            self.logger.erro(f"断言失败{str(e)}")
            filename = self.co.save_screen_shot(driver)
            if filename:
                allure.attach.file(filename, '异常截图', allure.attachment_type.PNG)
                raise


def asserIn_new(logger, co, first, second, msg=None,driver=None):
    '''
    判断first包含在second,否则抛错msg
    :param first:
    :param second:
    :param msg:
    :param driver:
    :return:
    '''
    try:
        assert first in second,msg
    except Exception as e:
        logger.erro(f"断言失败{str(e)}")
        filename = co.save_screen_shot(driver)
        if filename:
            allure.attach.file(filename, '异常截图', allure.attachment_type.PNG)
            raise

