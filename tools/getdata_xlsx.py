# -*- coding:utf-8 -*-
"""
 @Time:2021/3/24 21:38
 @Auth: Chh
 @Function:请输入....
"""

from tools.pdmtd import Pdmethod
class Rc(Pdmethod):

    def __init__(self):
        super().__init__()

    def pd_excel(self,testcase_name = None):
        """
        excel测试用例提取
        :param testcase_name: 传入对应用例名
        :return:
        """

        self.getinfodata()
        self.gettitle()

        file_list = []
        for info in self.file_data:
            file_list.append(dict(zip(self.file_title,info)))

        caselist = [info for info in file_list if info.get('用例函数名') == testcase_name]
        # caselist = [list(info.values())[:-1]for info in caselist] #二层列表
        print(caselist)


        return caselist