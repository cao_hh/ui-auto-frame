#!/usr/bin/env python
# -*- coding:utf-8 -*-

import pytest
from tools.common import *
import os
import shutil
import datetime

def copy_dir(src_path, target_path):
    '''
    将生成的allure报告复制到D盘tomcat下，通过连接可以直接访问各个报告
    回头需要将这些访问路径直接丢到测试平台上使用
    :param src_path:
    :param target_path:
    :return:
    '''
    if os.path.isdir(src_path) and os.path.isdir(target_path):
        filelist_src = os.listdir(src_path)
        for file in filelist_src:
            path = os.path.join(os.path.abspath(src_path), file)
            if os.path.isdir(path):
                path1 = os.path.join(os.path.abspath(target_path), file)
                if not os.path.exists(path1):
                    os.mkdir(path1)
                copy_dir(path, path1)
            else:
                with open(path, 'rb') as read_stream:
                    contents = read_stream.read()
                    path1 = os.path.join(target_path, file)
                    with open(path1, 'wb') as write_stream:
                        write_stream.write(contents)

        return 'allure报表文件复制成功'
    else:
        return '文件夹路径不存在'


if __name__ == '__main__':
    co = Common()
    allure_report,get_date = co.get_allure_dir()
    print(allure_report)
    print(get_date)

    '''清除昨天日期的报告'''
    today = datetime.date.today()
    oneday = datetime.timedelta(days=1)
    yesterday = today - oneday
    try:
        shutil.rmtree(f'./report/{yesterday}')
    except Exception:
        pass

    '''用例执行前清除screenShot目录的异常截图图片'''
    path = f'./screenShot'
    if os.path.exists(path):
        if len(os.listdir(path)) <= 1:
            shutil.rmtree(f"{path}/{''.join(os.listdir(path))}")
        else:
            for info in os.listdir(path):
                shutil.rmtree(f"{path}/{info}")
    else:
        os.makedirs(path)

    '''用例执行前，清空report目录下的报告以及xml目录下的文件'''

    shutil.rmtree(f'./report/{get_date}') #删除今天报告
    os.system(f"allure generate ./report/ -o ./report/xml/ --clean") #清空report报告下的xml文件

    '''启动pytest，-v是打印log的，-n 2意思是启动两个线程组执行用例'''
    # pytest.main(['-s','-v','-n','2','testcases/test_login.py', '--alluredir', "./report/xml"])

    '''--reruns,1   是指用例失败后，会将失败用例重新运行一次'''
    # pytest.main(['-s','-v',"--reruns","1",'testcases','--alluredir',"./report/xml"])

    '''如果想要跑多个test_*.py文件，就将test_login.py去掉'''
    pytest.main(['-s', '-v', 'testcases', '--alluredir', "./report/xml"])#启动pytest，执行测试用例

    '''将environment.properties文件复制到report/xml下，该文件是allure的环境变量配置文件'''
    shutil.copy("./report/environment.properties","./report/xml")

    '''将categories.js文件复制到xml下，该文件是allure报告环境分类配置'''
    shutil.copy("./report/categories.js","./report/xml")

    os.system("allure generate report/xml -o %s"%(allure_report)) #生成allure报告

    '''将生成的allure报告复制到D盘tomcat下'''
    target_patha = r"D:\program files\apache-tomcat-9.0.41\webapps\\allure_report"
    if not os.path.exists(target_patha):
        os.mkdir(target_patha)

    del_list = os.listdir(target_patha) #将D盘allure_report文件全部清空
    for f in del_list:
        file_path = os.path.join(target_patha, f)
        if os.path.isfile(file_path):
            os.remove(file_path)
        elif os.path.isdir(file_path):
            shutil.rmtree(file_path)
    sign = copy_dir( r'./report/',target_patha) #再将生成的新报告复制到D盘

    print(f'==================将报告复制到tomcat下，复制结果为：{sign}==================')

