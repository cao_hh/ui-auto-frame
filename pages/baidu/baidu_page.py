# -*- coding:utf-8 -*-
'''auhor: chh'''

from pages.pubilc.pg_public import *


class Baidu(Public):
    '''
    todo:页面元素及页面方法
    '''

    def __init__(self, browser):
        super().__init__(browser=browser)

        '''百度元素'''
        self.ipt_baidu = (By.CSS_SELECTOR, '#kw')  # 获取个人登录元素
        self.btn_baidu = (By.CSS_SELECTOR,'.bg.s_btn') #选择登录模式


    def bd_text(self,input):
        '''
        todo:百度输入框和按钮
        :return:
        '''
        self.send_text(self.ipt_baidu,input)
        self.click_element(self.btn_baidu)

        element = self.find_element_and_wait((By.CSS_SELECTOR,'[id="container"]'))

        return element
