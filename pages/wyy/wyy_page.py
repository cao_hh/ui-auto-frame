# -*- coding:utf-8 -*-
'''
auhor:
'''
from pages.pubilc.pg_public import *


class Wyy(Public):
    '''
    todo:党员管理 基础信息 页面元素及页面方法
    '''

    def __init__(self, browser):
        super().__init__(browser=browser)

        '''网易云元素'''
        self.btn_wyy_login = (By.CSS_SELECTOR, '.m-tophead>.s-fc3')  # 获取个人登录元素
        self.btn_wyy_login_mode = (By.CSS_SELECTOR,'#otherbtn>.u-btn2') #选择登录模式
        self.btn_wyy_login_input = (By.CSS_SELECTOR,'.n-log2')#登录界面输入手机号密码以及登录按钮的父级
        self.link_wyy_musicapp = (By.CSS_SELECTOR,'.download-link') #获取登录界面的网易云音乐app元素
        self.btn_wyy_check = (By.CSS_SELECTOR,'.u-official-terms')#勾选的父级

        self.cls_wyy_login = (By.CSS_SELECTOR,'.login-list')#获取登录界面父级元素
        self.search_input = (By.CSS_SELECTOR,'.srchbg input') # 主页搜索框
        self.wyytitle_text = (By.CSS_SELECTOR,'.srchsongst>div:first-child .s-fc7') # 搜索后的界面

    def wyy_text_login(self):
        '''
        todo:网易云登录字段获取
        :return:
        '''
        hwnd = self.find_element_and_wait(self.btn_wyy_login)

        return hwnd


    def wyy_login(self,username,passwd):
        '''
        todo:网易云个人登录
        :return:
        '''
        '''切换到个人登录界面'''
        self.findElement(self.btn_wyy_login).click()
        time.sleep(0.3)

        step = 0
        while True:
            time.sleep(0.3)
            js = f'return document.querySelector(".z-show")'
            hwnd = self.driver.execute_script(js)
            step += 1
            if hwnd:
                self.findElement(self.btn_wyy_login_mode).click()
                time.sleep(0.3)
                break
            elif step == 30:
                return False

        self.driver.find_element_by_css_selector('.u-alt+.u-official-terms>input').click()
        time.sleep(0.3)
        self.driver.find_element_by_css_selector('.u-plt+.f-mgt10>a').click()
        time.sleep(0.2)

        '''个人登录：手机号，密码，登录按钮'''
        hwnd_login = self.findElements(self.btn_wyy_login_input)
        for i in hwnd_login:
            wyy_phone = i.find_element_by_css_selector('.txtwrap>input')
            wyy_phone.send_keys(username)
            wyy_passwd = i.find_element_by_css_selector('.f-mgt10>input')
            wyy_passwd.send_keys(passwd)

            wyy_login = i.find_element_by_css_selector('.f-mgt20>a')
            wyy_login.click()

        # 定位设置模拟鼠标悬停动作(定位原始位置)
        abv = self.driver.find_element_by_css_selector('.m-tophead.j-tflag')

        ActionChains(self.driver).move_to_element(abv).perform()
        # 模拟鼠标拖放动作（定位目标位置）

        element = self.find_element_and_wait((By.LINK_TEXT,'我的主页'))

        if element:
            return element.text,True
        else:
            ele = self.find_element_and_wait((By.CSS_SELECTOR,'.j-err.u-err'))
            if ele:
                return ele.text,False

    def wyy_search(self,text):
        """
        todo:主页搜索框
        :param text: 搜索关键词
        :return:
        """

        self.send_text(self.search_input,text)
        self.enter(self.findElement(self.search_input))

        self.switch_frame("contentFrame")
        search_text = self.find_element_and_wait(self.wyytitle_text)

        return search_text.text

