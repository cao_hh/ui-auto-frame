# -*- coding:utf-8 -*-


import traceback
import hashlib

from collections import defaultdict

from utils.mysqlmtd import MysqlConn
from utils.pdmysql import Pdmysql


class Publicsql(MysqlConn, Pdmysql):
    def __init__(self, user):
        super(Publicsql, self).__init__(user)
        super(MysqlConn, self).__init__(user)

        self.areaid = defaultdict(dict)  # eg: {'武汉市': '430000'}
        self.areaname = defaultdict(dict)  # eg: {'430030': '湖北省武汉市硚口区XXXXXX'}
        self.orgname = defaultdict(dict)  # eg:{'长滩镇长滩社区党支部': '001001'}
        self.md = hashlib.md5()

        self.table_info = self._get_tableinfo()  # 数据库表名称对应字段信息
        self.dictdetail_info = self._get_dictdetail()  # 数据字典名称对应value信息
        self.data_dict = self._get_datadict()  # 数据字典名称对应key信息

    def _get_dictdetail(self):
        '''
        获取数据库字典数据, 并初始化
        :return:# {'whcd': {'1': '文盲'}}
        '''

        datadict = defaultdict(dict)
        sql_temp = "SELECT a.dict_value, a.dict_name, b.code " \
                   "from sys_dictionary_detail a LEFT JOIN sys_dictionary b on a.dictionary_id = b.id"

        self.cur.execute(sql_temp)
        data_dt_list = self.cur.fetchall()

        for data_dt in data_dt_list:
            datadict[data_dt[2]][data_dt[0]] = data_dt[1]

        return datadict

    def _get_tableinfo(self):
        '''
        查询数据库中所有的表名 以及表字段
        :return:
        '''

        sql_temp = "select table_name from information_schema.tables where table_schema='{}'".format(
            self.db['database'])
        self.cur.execute(sql_temp)
        tablist = self.cur.fetchall()
        sys_tablist = [tab[0] for tab in tablist if tab[0][-1] not in '0123456789']
        del tablist

        tabobj = dict()
        for tab in sys_tablist:
            # 动态创建类
            table = type('Table', (), {})()
            sql_temp = "select column_name, column_comment from information_schema.columns where table_schema ='{}'  " \
                       "and table_name = '{}';".format(self.db['database'], tab)
            self.cur.execute(sql_temp)
            tabinfo = self.cur.fetchall()
            for info in tabinfo:
                table.__setattr__(info[0], None)
            tabobj[tab] = table
        return tabobj

    def _get_datadict(self):
        '''
        获取数据字典基础信息
        :return: {'单位层级' : 'unitLevel'}
        '''

        sql_temp = "SELECT name, code FROM sys_dictionary;"

        self.cur.execute(sql_temp)
        datadict = self.cur.fetchall()
        datadict = dict(datadict)

        return datadict

    def get_areaid_byname(self, name):
        '''
        获取行政区划, 行政区划名字转id
        :param name:
        :param sql:
        :return:
        '''
        try:
            data = self.areaid[name]['code']
            return data

        except KeyError:
            sql = "select id from sys_area where name like '%{}%' and id like '{}%'" \
                .format(name, self.config.get('base_areacode'))
            self.cur.execute(sql)
            data = self.cur.fetchone()
            if not data:
                raise ValueError(f"{name} 行政地区不存在")
            self.areaid[name]['code'] = ''.join(data)
            return self.areaid[name]['code']

    def get_areaname_byid(self, code, name=''):
        '''
        获取行政区划, id转名称
        :return:
        '''

        src_code = code
        try:
            data = self.areaname[src_code]['name']
            return data

        except KeyError:
            while True:
                sql = f"SELECT id, name from sys_area a inner JOIN " \
                    f"(SELECT parent_id from sys_area where id = '{code}') b " \
                    f"where a.id = b.parent_id;"

                self.cur.execute(sql)
                data = self.cur.fetchone()
                if not data:
                    self.areaname[src_code]['name'] = name
                    return self.areaname[src_code]['name']
                code = data[0]
                name = data[1] + name

    def get_partyorgid_byname(self, orgname):
        '''
        获取党组织id
        :return:
        '''

        try:
            data = self.orgname[orgname]['id']
            return data

        except KeyError:
            sql = "SELECT id from sys_party_organization where name = '{}';".format(orgname)
            self.cur.execute(sql)
            data = self.cur.fetchone()
            if not data:
                raise ValueError(f"{orgname} 党组织不存在")
            self.orgname[orgname]['id'] = ''.join(data)
            return self.orgname[orgname]['id']

    def get_childcode_bycode(self, areacode):
        '''
        获取下级所有的行政区划code
        :return:
        '''

        sql_temp = f"select id from sys_area where id like '{areacode}%'"
        self.cur.execute(sql_temp)
        childcode_list = self.cur.fetchall()

        return childcode_list

    def get_childorg_byname(self, orgname):
        '''
        获取下级所有党组织id
        :param orgname:
        :return:
        '''
        sign = True

        rst_orgs = []

        sql_temp = f"select id from sys_party_organization where name like '{orgname}%'"
        self.cur.execute(sql_temp)
        org_ids = self.cur.fetchall()

        while sign:
            temp_org = []
            for org_id in org_ids:
                sql_temp = f"select id from sys_party_organization where parent_id = '{org_id[0]}'"
                self.cur.execute(sql_temp)
                datas = self.cur.fetchall()
                temp_org += list(datas)

            if not len(temp_org):
                rst_orgs += org_ids
                break
            else:
                org_ids = temp_org

        return [rst_org[0] for rst_org in rst_orgs]

    def get_fullareainfo(self, areaid):
        '''
        通过areaid 获取完整的行政区划名称
        :return: ['湖北省', '荆门市', '钟祥市', '长寿镇', '长寿村']
        '''

        areainfo = []

        while True:
            sql_temp = f"select id, parent_id, name , type from sys_area where id = '{areaid}'"
            self.cur.execute(sql_temp)
            data = self.cur.fetchone()
            areainfo.insert(0, data[2])
            if data[3] == '1':
                break
            areaid = data[1]

        return areainfo

    def get_peoinfo_sum(self):
        '''
        搜索居民档案的总数
        :return:
        '''
        sql_temp = "select count(*) from app_resident WHERE file_status != '2'"
        self.cur.execute(sql_temp)
        data = list(map(lambda x:x[0],self.cur.fetchall()))[0]

        return data

