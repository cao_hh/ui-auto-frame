# -*- coding:utf-8 -*-



import re
import random
from functools import reduce

from pages.pubilc.pg_public import *


class Draft(Public):
    '''
    居民基础信息 页面元素及页面方法
    '''

    def __init__(self, browser):
        super().__init__(browser=browser)

        self.btn_povertyadd = (By.CSS_SELECTOR, '.operationAddBtn .el-button.el-button--primary')

        self.btn_welcome = (By.CSS_SELECTOR, '[aria-label="总说明"] .el-button.el-button--primary')
        self.btn_edits = (By.CSS_SELECTOR, '[title="编辑"]')
        self.ipt_datas = (By.CSS_SELECTOR, '.tableList-content input[type="text"]')

        self.btn_page = (By.CSS_SELECTOR, '.pagination .el-select__caret.el-input__icon.el-icon-arrow-up')

        # 武汉农委
        # self.row_peotitle = (By.CSS_SELECTOR, '.main-list .el-table__header-wrapper [class*=el-table]>div') #农户标题栏
        self.btn_peoedits = (By.CSS_SELECTOR, '.main-list .el-table__row.table-row [title = 编辑]')  # 编辑按钮

        # 荣昌数据填报 详情页相关
        self.ipt_enables = (By.CSS_SELECTOR, '.tableList-content tbody tr input:enabled')  # 可填写的ipt框
        self.row_ipts = (By.CSS_SELECTOR, '.tableList-content tbody tr')  # 输入行

        self._init_mathfunc()

    def _init_mathfunc(self):
        '''
        初始化公式方法
        :return:
        '''

        self.mathre = {}
        self.mathre['re_div'] = re.compile('(.*\d\/[^\+\-\*\/]*\d)')
        self.mathre['re_mul'] = re.compile('(.*\d\*[^\+\-\*\/]*\d)')
        self.mathre['re_add'] = re.compile('(.*\d\+[^\+\-\*\/]*\d)')
        self.mathre['re_sub'] = re.compile('(.*\d\-[^\+\-\*\/]*\d)')

        self.mathfunc = {}
        self.mathfunc['re_div'] = lambda x, y: f'{round(x / y, 2):.2f}'
        self.mathfunc['re_mul'] = lambda x, y: f'{round(x * y, 2):.2f}'
        self.mathfunc['re_add'] = lambda x, y: f'{round(x + y, 2):.2f}'
        self.mathfunc['re_sub'] = lambda x, y: f'{round(x - y, 2):.2f}'
        self.mathfunc['/'] = lambda x, y: round(x / y, 2)
        self.mathfunc['*'] = lambda x, y: round(x * y, 2)
        self.mathfunc['+'] = lambda x, y: round(x + y, 2)
        self.mathfunc['-'] = lambda x, y: round(x - y, 2)

    def send_ipt_vals(self):
        '''
        数据上报填写数据
        :return:
        '''

        try:
            hwnd = self.findElement(self.btn_welcome)
            hwnd.click()
        except:
            pass

        time.sleep(0.5)

        hwnd = self.findElement(self.btn_page)
        # js = r"return arguments[0].parentNode;"
        # hwnd_item = self.driver.execute_script(js, hwnd)
        hwnd.click()
        self.chose_select('200条/页')

        time.sleep(0.3)
        hwnds = self.findElements(self.btn_edits)
        # 此处选择表格索引位置
        hwnd = hwnds[0]
        js = 'arguments[0].scrollIntoView()'
        self.driver.execute_script(js, hwnd)
        hwnd.click()
        time.sleep(0.3)

        hwnd_ipts = self.findElements(self.ipt_datas)
        for ipt in hwnd_ipts:
            sign = ipt.get_attribute('disabled')
            if not sign:
                num = random.uniform(0, 99)
                ipt.send_keys(str(round(num, 2)))

    def clear_welcome(self):
        '''
        清除总说明
        :return:
        '''

        try:
            hwnd = self.findElement(self.btn_welcome)
            hwnd.click()
        except:
            pass

    def switch_reporttab(self, areanema):
        '''
        切换上报表格
        :return:
        '''

        time.sleep(0.5)
        tabelements = self.get_tabelements()

        for elem in tabelements:
            if areanema in elem.get('报表名称')[0]:
                hwnd = elem.get('操作')[1]
                btn = hwnd.find_element_by_css_selector('[title=查看详情]')
                btn.click()
                break
        time.sleep(0.2)

    def switch_reportpeo(self, idx):
        '''
        武汉农委, 切换户主填报表格
        :return:
        '''

        time.sleep(0.3)
        btn_edit = self.findElements(self.btn_edits)[idx]
        btn_edit.click()

    def send_ipt_vals_tjtb(self):
        '''
        统计填报填写数据
        :return:
        '''
        time.sleep(1)
        ipts = self.findElements(self.ipt_enables)
        for ipt in ipts:
            num = random.uniform(0, 999)
            ipt.send_keys(str(round(num, 2)))

    def check_countresult_row(self, formula_info):
        '''
        检查计算结果
        :return: {1:[2,3,4], 6:[7,8,9]}
        '''

        # 行结果计算
        row_ipts = self.findElements(self.row_ipts)
        for row, rowlist in formula_info.items():
            print(f'========= 判断行号为 [{row}] 的计算是否正确 \n')
            row_hwnd = row_ipts[row]
            iptvals = row_hwnd.find_elements_by_css_selector('input')
            iptvals = [val.get_attribute('value') for val in iptvals]

            rstiptvals = []
            for temprow in rowlist:
                temprow_hwnd = row_ipts[temprow]
                tempiptvals = temprow_hwnd.find_elements_by_css_selector('input')
                rstiptvals.append([val.get_attribute('value') for val in tempiptvals])

            for idx, val in enumerate(iptvals):
                showtotle = val
                rsttotle = 0
                showtemp = []
                for temp in rstiptvals:
                    try:
                        rsttotle += float(temp[idx])
                    except ValueError:
                        # print(temp[idx])
                        pass
                    showtemp.append(temp[idx])
                rsttotle = f'{round(rsttotle, 2):.2f}'

                print(f'前端页面显示数据为:  {showtotle}')
                print(f'前端数据相加的数值列表为:  {str(showtemp)}')
                print(f'实际计算总值为: {rsttotle}')
                print(f'===== {["合计错误", "合计正确"][showtotle.strip() == str(rsttotle)]} =====')

    def check_countresult_col(self, methods):
        '''
        检查计算结果, 武汉农委
        :return:
        '''

        file = open('checkrst.txt', 'w')
        for idx, method in enumerate(methods):
            file.write(f'处理第 [{idx}] 条公式 [ {method} ] \n')
            # 处理公式
            self.idx = 0
            rst_data, formula = re.split('=', method)
            rst_data = rst_data.strip().replace(' ', '')
            formula = formula.strip().replace(' ', '')
            re_bracket = re.compile('\((?P<data>[^()]+)\)')

            def process(matched):
                self.idx += 1
                rst[f'rp_{self.idx}']['formula'] = matched.group('data')
                return f'rp_{self.idx}'

            rst = defaultdict(dict)
            formula = re.sub(re_bracket, process, formula)

            row_ipts = self.findElements(self.row_ipts)
            for row_idx, row in enumerate(row_ipts):
                file.write(f' 目前处理 行[ {row_idx} ] 的数据')

                # 页面展示的结果数据
                try:
                    hwnd_rsdata = row.find_element_by_css_selector(f'td:nth-child({rst_data}) input')
                    data_rsdata = float(hwnd_rsdata.get_attribute('value'))
                    data_rsdata = f'{round(data_rsdata, 2):.2f}'
                except NoSuchElementException:
                    continue

                # 处理括号最高优先级的运算
                rp_list = []
                for key, val in rst.items():
                    for name, func in self.mathre.items():
                        try:
                            temp = func.match(val.get('formula')).group()
                            rowa, rowb = re.split('\W', temp)
                            hwnd_rowa = row.find_element_by_css_selector(f'td:nth-child({rowa}) input')
                            data_rowa = float(hwnd_rowa.get_attribute('value'))
                            data_rowa = f'{round(data_rowa, 2):.2f}'
                            hwnd_rowb = row.find_element_by_css_selector(f'td:nth-child({rowb}) input')
                            data_rowb = float(hwnd_rowb.get_attribute('value'))
                            data_rowb = f'{round(data_rowb, 2):.2f}'
                            rp_list.append(self.mathfunc.get(name)(float(data_rowa), float(data_rowb)))
                        except AttributeError:
                            continue

                # 将括号替换的最终公式进行处理
                paras = re.split('([\+\-\*\/=])', formula)
                temp_funcsign = [data for idx, data in enumerate(paras) if idx % 2 == 1]  # 符号列表
                paras = [data for data in paras if data not in temp_funcsign]  # 参数列表
                iterfunc = iter(temp_funcsign)

                new_paras = []
                for para in paras:
                    # 替换的符号取值
                    if para.startswith('rp_'):
                        idx = int(para.replace('rp_', ''))
                        new_paras.append(float(rp_list[idx]))
                    else:
                        hwnd_row = row.find_element_by_css_selector(f'td:nth-child({para}) input')
                        data = float(hwnd_row.get_attribute('value'))
                        data = round(data, 2)
                        new_paras.append(data)

                def tempfunc(x, y):
                    method = next(iterfunc)
                    return self.mathfunc.get(method)(x, y)

                rowrst = reduce(tempfunc, new_paras)
                file.write(f' 页面为 {data_rsdata} 实际为 {str(rowrst)} \n')
        file.close()



