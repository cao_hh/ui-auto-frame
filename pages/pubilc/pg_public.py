# -*- coding:utf-8 -*-



import re
from pages.base import *
from collections import defaultdict


class Public(Base):
    '''
    登录页 及 首页 元素定位及方法
    '''

    def __init__(self, browser):
        super().__init__(browser=browser)

        self.ipt_login = (By.CSS_SELECTOR, 'input[placeholder="请输入您的手机号"]')  # 登录按钮
        self.ipt_password = (By.CSS_SELECTOR, 'input[placeholder="请输入您的密码"]')  # 输入密码框
        self.btn_login = (By.CSS_SELECTOR, '.el-button.login-btn.el-button--primary')  # 登录按钮
        self.link_fgtpassword = (By.CSS_SELECTOR, '.reset-password-row')  # 忘记密码
        self.img_peoinfo = (By.CSS_SELECTOR, '.headerNav .el-avatar.el-avatar--circle')  # 主页个人头像
        self.btn_peocenter = (By.CSS_SELECTOR, 'body>ul[class*=el-dropdown-menu] li')  # 个人中心操作扭

        self.btn_circleclose = (By.CSS_SELECTOR, '[class*=circle-close]')  # 全局圆形x按钮

        # 左侧 menu 标签
        '''
        # 老版本 不可用
        self.menu_gzt = (By.CSS_SELECTOR, '.ai-menu-padding .el-tooltip.ai-submenu:nth-child(1)')  # 工作台
        self.menu_ysz = (By.CSS_SELECTOR, '.ai-menu-padding .el-tooltip.ai-submenu:nth-child(7)')  # 系统设置
        self.menu_djdw = (By.CSS_SELECTOR, '.ai-menu-padding .el-tooltip.ai-submenu:nth-child(3)')  # 党建党务
        self.menu_zwyy = (By.CSS_SELECTOR, '.ai-menu-padding .el-tooltip.ai-submenu:nth-child(4)')  # 政务应用
        self.menu_bmfw = (By.CSS_SELECTOR, '.ai-menu-padding .el-tooltip.ai-submenu:nth-child(5)')  # 便民服务
        self.menu_tsrq = (By.CSS_SELECTOR, '.ai-menu-padding .el-tooltip.ai-submenu:nth-child(6)')  # 特殊人群
        self.menu_tbtj = (By.CSS_SELECTOR, '.ai-menu-padding .el-tooltip.ai-submenu:nth-child(2)>li')  # 统计填报
        self.menu_dwxx = (By.CSS_SELECTOR, '.ai-menu-padding .el-tooltip.ai-submenu:nth-child(3)')  # 阳光村务_党务信息
        self.menu_zwxx = (By.CSS_SELECTOR, '.ai-menu-padding .el-tooltip.ai-submenu:nth-child(4)')  # 阳光村务_政务信息
        '''

        # 新版本 菜单定位
        # 民政项目菜单定位
        self.menu_gzt = (By.CSS_SELECTOR, '.ai-submenu:nth-child(1)')  # 工作台
        self.menu_xtsz = (By.CSS_SELECTOR, '.ai-submenu:nth-child(7)')  # 系统设置
        self.menu_shjz = (By.CSS_SELECTOR, '.ai-submenu:nth-child(3)')  # 社会救助
        self.menu_xxgl = (By.CSS_SELECTOR, '.ai-submenu:nth-child(4)')  # 信息管理
        self.menu_jmxx = (By.CSS_SELECTOR, '.ai-submenu:nth-child(5)')  # 居民信息
        self.menu_sjgx = (By.CSS_SELECTOR, '.ai-submenu:nth-child(6)')  # 数据共享

        # 数字乡村菜单定位
        self.menu_jcbg = (By.CSS_SELECTOR, '.ai-submenu:nth-child(4)')  # 基层办公
        self.menu_rjhj = (By.CSS_SELECTOR, '.ai-submenu:nth-child(7)')  # 人居环境

        self.second_menu = (By.CSS_SELECTOR, '.ai-menu-item')  # 二级菜单

        # tag页
        self.tag_titles = (By.CSS_SELECTOR, '[role="tablist"]>div')  # 页面tag页签

        # panel页
        self.panel_titles = (By.CSS_SELECTOR, '.main-tab p')  # panel页签

        # 人员选择器
        self.btn_select_peop = (By.CSS_SELECTOR, '.AiPersonSelect')  # 选择人员按钮
        self.tabrow_select_peop = (By.CSS_SELECTOR, '.seleceUl li')  # 居民列表人员
        self.btn_peoselect_submit = (By.CSS_SELECTOR, '.el-dialog__body+div button')  # 点击选中人员界面的确定按钮

        # 下拉菜单
        self.sele_items = (By.CSS_SELECTOR, '[x-placement*=-start] li')  # 下拉框选择项定位
        self.sele_dropitems = (By.CSS_SELECTOR, '.el-dropdown-menu.el-popper li')  # 下拉菜单定位

        # 左侧树相关
        self.tag_topitems = (By.CSS_SELECTOR, 'div[role="tree"]>div[role="treeitem"]>.el-tree-node__content')  # 一级树tag
        self.tag_nextitems = (By.CSS_SELECTOR, 'div[role=group] div[role=treeitem]')  # 次级目录树tag

        # 新增修改输入框定位
        self.lab_ipts = (By.CSS_SELECTOR, '[class*=content] .el-form-item>label')  # 输入框label定位

        # 详情页面字段定位
        self.lab_iptvals = (By.CSS_SELECTOR, '[class*=info] .label')  # 详情页面label
        self.lab_peoname = (By.CSS_SELECTOR, 'h3.name')  # 姓名
        self.picture = (By.CSS_SELECTOR, '.image-box')  # 个人头像

        # 高级搜索定位相关
        self.ipts_searchs = (By.CSS_SELECTOR, '.searchBar input')  # 高级搜索输入框
        self.btn_downshape = (By.CSS_SELECTOR, '.down .down-content')  # 展开高级搜索按钮
        self.icon_search = (By.CSS_SELECTOR, '.searchBar button .iconfont.iconSearch')  # 查询
        self.icon_refresh = (By.CSS_SELECTOR, '.searchBar button .el-icon-refresh-right')  # 重置

        # 地区切换相关
        self.btn_chosearea = (By.CSS_SELECTOR, '.ai-area')  # 切换地区按钮
        self.div_areabox = (By.CSS_SELECTOR, '.area_edge .area-box')  # 地区选择区域
        self.btn_areasubmit = (By.CSS_SELECTOR, '.el-dialog.ai-dialog .el-button.el-button--primary')  # 确定

        # 必填项信息相关
        self.lab_mustfill = (By.CSS_SELECTOR,
                             '.el-form-item.is-required:not(.is-no-asterisk) '
                             '.el-form-item__label-wrap>.el-form-item__label, .el-form-item.is-required:not('
                             '.is-no-asterisk)>.el-form-item__label')  # 获取所有必填项lable
        self.lab_mustfill_err = (By.CSS_SELECTOR, '[class*=is-error]>label')  # 必填项未填写时, 错误区域

        # 列表页相关
        self.div_table = (By.CSS_SELECTOR, '.table_list')  # 表格div主体
        self.lab_tabtitles = (
            By.CSS_SELECTOR,
            '.el-table__header-wrapper th[class*=el-table]:not(.el-table-column--selection) div')  # 列表标题
        self.row_tabdatas = (By.CSS_SELECTOR, '.el-table__body-wrapper tr')  # 列表行
        # self.div_listpage = (By.CSS_SELECTOR, '.pagination')  # 列表底部page区域
        self.div_listpage = (By.CSS_SELECTOR, '[class*=pagination]')  # 列表底部page区域

        # 头像图片相关
        self.btn_picture = (By.CSS_SELECTOR, '[class="upload-box"] button>span')
        self.div_picture = (By.CSS_SELECTOR, '.image-box')
        self.lab_markword = (By.CSS_SELECTOR, '.upload-tip')

        # 选择人员
        self.btn_peo = (By.CSS_SELECTOR, '.AiPersonSelect button')

        # 富文本iframe 相关
        self.body_content = (By.CSS_SELECTOR, 'iframe[id*=ueditor]')

        # 右上角功能按钮
        self.btn_downloadcenter = (By.CSS_SELECTOR, '.headerNav .iconDownload')  # 下载中心

        # 下载中心相关
        self.row_downloaditems = (By.CSS_SELECTOR, '.downLoad_main .infinite-list-item')  # 下载中心数据条目
        self.btn_downloadclose = (By.CSS_SELECTOR, '[aria-label="下载中心"] .el-icon-close')  # 关闭下载中心

        self._iptfunc_switch()

    def _iptfunc_switch(self):
        '''
        初始化输入方法
        :return:
        '''

        self.iptfuncs = {}
        self.iptfuncs['text'] = lambda item, data: item.send_keys(data) if data != 'CLEAR' else item.clear()
        self.iptfuncs['number'] = lambda item, data: item.send_keys(data) if data != 'CLEAR' else item.clear()
        self.iptfuncs['slct_spec'] = lambda item, data: item.send_keys(data) if data != 'CLEAR' else item.clear()
        self.iptfuncs['slct'] = lambda item, data: self.chose_select(data) if data != 'CLEAR' else item.clear()
        self.iptfuncs['checkbox'] = lambda item, data: self.chose_radioORchkbox(item, data)
        self.iptfuncs['radio'] = lambda item, data: self.chose_radioORchkbox(item, data)
        self.iptfuncs['textarea'] = lambda item, data: item.send_keys(data) if data != 'CLEAR' else item.clear()

    def login(self,host=None):
        '''
        登录
        :return:
        '''

        if host:
            url = self.config.get(host)
        else:
            url = self.config.get('host')
        self.driver.get(url)

        # hwnd = self.findElement(self.ipt_login)
        # hwnd.send_keys(self.config.get(f'username_{owner}'))
        #
        # hwnd = self.findElement(self.ipt_password)
        # hwnd.send_keys(self.config.get(f'password_{owner}'))
        #
        # hwnd = self.findElement(self.btn_login)
        # hwnd.click()

    def logout(self):
        '''
        登出
        :return:
        '''

        time.sleep(0.3)
        hwnd_img = self.findElement(self.img_peoinfo)
        hwnd_img.click()

        time.sleep(0.3)
        hwnd_btns = self.findElements(self.btn_peocenter)
        hwnd_btns[-1].click()

    def switch_module(self, modulename):
        '''
        切换模块
        :param modulename:
        工作台: gzt; 系统设置: ysz; 党建党务: djdw; 政务应用: zwyy; 便民服务: bmfw
        :return:
        '''

        time.sleep(0.5)

        switch = {}
        switch['工作台'] = 'gzt'
        switch['系统设置'] = 'xtsz'
        # switch['党建党务'] = 'djdw'
        # switch['政务应用'] = 'zwyy'
        # switch['便民服务'] = 'bmfw'
        # switch['特殊人群'] = 'tsrq'
        # switch['填报统计'] = 'tbtj'
        # switch['党务信息'] = 'dwxx'
        # switch['政务信息'] = 'zwxx'
        switch['数据共享'] = 'sjgx'
        switch['社会救助'] = 'shjz'
        switch['基层办公'] = 'jcbg'
        switch['人居环境'] = 'rjhj'

        modules = {key: val for key, val in self.__dict__.items() if key.startswith('menu_')}
        hwnd = self.findElement(modules.get(f'menu_{switch.get(modulename)}'))
        hwnd.click()

    def switch_menu(self, menuname):
        '''
        切换二级菜单
        :param menuname: 二级菜单名称
        :return:
        '''

        time.sleep(0.5)
        menus = self.findElements(self.second_menu)
        father_div = self.findParentNode(menus[0])
        for idx, menu in enumerate(menus):
            if menu.text.strip() == menuname:
                js = f'arguments[0].scrollTop = {idx} * arguments[0].scrollHeight/{len(menus)}'
                self.driver.execute_script(js, father_div)
                js = 'arguments[0].scrollIntoView()'
                self.driver.execute_script(js, menu)
                menu.click()
                break

    def switch_tag(self, tagname):
        '''
        切换tag页签
        :return:
        '''

        time.sleep(0.5)
        tags = self.findElements(self.tag_titles)
        for tag in tags:
            if not tag.text.strip():
                continue
            if tagname in tag.text.strip():
                tag.click()
                break

    def switch_panel(self, panelname):
        '''
        切换panel页签
        :param panelname:
        :return:
        '''

        time.sleep(0.5)
        panels = self.findElements(self.panel_titles)
        for p in panels:
            title = p.text.strip()
            if not title:
                continue
            if panelname in title:
                p.click()
                break

    # def chose_people(self, sign):
    #     '''
    #     选择人员器
    #     :param sign:  取消; 确定
    #     :return:
    #     '''
    #
    #     time.sleep(0.5)
    #     hwnd = self.findElement(self.btn_select_peop)  # 点击选择人员按钮
    #     hwnd.click()
    #     time.sleep(0.5)
    #
    #     people = [x for x in self.findElements(self.tabrow_select_peop)]  # 获取每页居民人员
    #     hwnd = random.sample(people, 1)[0]
    #     hwnd.click()
    #     time.sleep(0.5)
    #
    #     hwnd = [i for i in self.findElements(self.btn_peoselect_submit) if i.text == sign]  # 点击选择人员确定按钮
    #     hwnd[0].click()
    #     time.sleep(0.5)

    def chose_select(self, item):
        '''
        选择下拉选项
        :param item: 选项名称 str 或 选项索引 int
        :return:
        '''

        time.sleep(0.5)

        sele_items = self.findElements(self.sele_items)

        if isinstance(item, int):
            time.sleep(0.2)
            hwnd_item = sele_items[item]
            hwnd_item.click()
        else:
            for hwnd_item in sele_items:
                if item == hwnd_item.text:
                    time.sleep(0.2)
                    hwnd_item.click()
                    break

    def chose_dropitems(self, item):
        '''
        选择下拉菜单
        :param item: 选项名称 str
        :return:
        '''

        time.sleep(0.5)

        sele_items = self.findElements(self.sele_dropitems)
        for hwnd_item in sele_items:
            if item == hwnd_item.text:
                time.sleep(0.2)
                hwnd_item.click()
                break

    def chose_radioORchkbox(self, item, idx):
        '''
        选择单选或者多选
        :param item: 任意一个选择框的任意对象,  idx: 需要选择的索引
        :return:
        '''

        time.sleep(0.5)
        while True:
            js = r"return arguments[0].tagName"
            tagname = self.driver.execute_script(js, item)
            if tagname == 'LABEL':
                js = r"return arguments[0].parentNode;"
                farther = self.driver.execute_script(js, item)
                break
            js = r"return arguments[0].parentNode;"
            item = self.driver.execute_script(js, item)

        label = farther.find_elements_by_css_selector('label')[int(idx) - 1]
        label.click()

    def switch_tree_toptag(self, tagname):
        '''
        选择顶层树目录
        :param 顶级目录树名称
        :return:
        '''

        time.sleep(0.5)
        toptags = self.findElements(self.tag_topitems)

        for toptag in toptags:
            if tagname in toptag.text:
                js = r"return arguments[0].parentNode;"
                hwnd_toptag = self.driver.execute_script(js, toptag)
                time.sleep(0.5)
                hwnd_toptag.click()
                return hwnd_toptag

    def switch_tree_nexttag(self, tagname):
        '''
        选择树下级目录
        :param tagname: 次级目录树名称
        :return:
        '''

        time.sleep(0.5)
        nexttags = self.findElements(self.tag_nextitems)

        for nexttag in nexttags:
            if tagname in nexttag.text:
                time.sleep(0.5)
                nexttag.click()
                return nexttag

    def switch_area(self, paras):
        '''
        切换地区
        :param paras 区域 {'县': '钟祥市', '镇': '长寿镇'}
        :return:
        '''

        time.sleep(0.5)
        hwnd = self.findElement(self.btn_chosearea)
        hwnd.click()
        time.sleep(0.5)

        for key, val in paras.items():
            div_areaboxs = self.findElements(self.div_areabox)
            div_areaboxs = [div for div in div_areaboxs if not div.get_attribute('style')]

            for div in div_areaboxs:
                label = div.find_element_by_css_selector('h2')
                if key not in label.text:
                    continue

                items = div.find_elements_by_css_selector('.area-item>.AiBadge')
                for item in items:
                    if val == item.text.strip():
                        item.click()
                        time.sleep(0.5)

        time.sleep(0.5)
        hwnd = self.findElement(self.btn_areasubmit)
        hwnd.click()

    def get_elems_labs(self):
        '''
        新增页面元素label 定位
        :return:
        '''

        time.sleep(0.5)
        elem_lab = {}
        hwnd_labels = self.findElements(self.lab_ipts)
        for lab in hwnd_labels:
            elem_lab[lab.text.strip('：')] = lab

        return elem_lab

    def get_diselems_labs(self):
        '''
        新增页面自动计算的元素 label 定位
        :return:
        '''

        time.sleep(0.5)
        elem_lab = {}
        hwnd_labels = self.findElements(self.lab_ipts)
        for lab in hwnd_labels:
            js = r"return arguments[0].parentNode;"
            hwnd_item = self.driver.execute_script(js, lab)
            hwnd_input = hwnd_item.find_element_by_css_selector('input')
            dis_sign = hwnd_input.get_attribute('disabled')
            if dis_sign:
                elem_lab[lab.text.strip('：')] = hwnd_input
        return elem_lab

    def get_elems_ipts(self, parentNode=None):
        '''
        页面元素定位及输入
        :return:
        '''

        # eg {'姓名':[{'text':'webelement'}], '地址':[{'slct':'webelement'}, {'slct':'webelement'}]}
        elem_func = defaultdict(list)

        '''
        找到所有输入项的标签, 查找父级, 找到所有输入项
        判断输入项类型, 并生成字典
        '''
        if parentNode:
            parentNode = self.findElement(parentNode)
        hwnd_labels = self.findElements(self.lab_ipts, parentNode)
        for lab in hwnd_labels:
            js = r"return arguments[0].parentNode;"
            hwnd_item = self.driver.execute_script(js, lab)

            item_name = lab.text.strip('：')
            js = "return arguments[0].querySelector('.el-form-item__content')"
            item = self.driver.execute_script(js, hwnd_item)
            js = "return arguments[0].querySelectorAll('input,textarea')"
            hwnd_ipts = self.driver.execute_script(js, item)
            for hwnd in hwnd_ipts:
                hwnd_type = hwnd.get_attribute('type')
                readonly_sign = hwnd.get_attribute('readonly')
                # 找到元素父级, 判断是否为时间输入控件
                js = r"return arguments[0].parentNode;"
                hwnd_father = self.driver.execute_script(js, hwnd)
                date_sign = True if 'date' in hwnd_father.get_attribute('class') else False

                if date_sign:
                    hwnd_type = 'slct_spec'
                elif hwnd_type == "text" and readonly_sign:
                    hwnd_type = 'slct'
                elem_func[item_name].append({hwnd_type: hwnd})

        if elem_func.__contains__(''):
            elem_func.pop('')
        return elem_func

    def get_elems_ipts_tolist(self, parentNode=None):
        '''
        页面元素定位及输入
        :return:
        '''

        # eg {'姓名':[('text','webelement')], '地址':[('slct','webelement'), ('slct','webelement')]}
        elem_func = defaultdict(list)

        '''
        找到所有输入项的标签, 查找父级, 找到所有输入项
        判断输入项类型, 并生成字典
        '''
        if parentNode:
            parentNode = self.findElement(parentNode)
        hwnd_labels = self.findElements(self.lab_ipts, parentNode)
        for lab in hwnd_labels:
            js = r"return arguments[0].parentNode;"
            hwnd_item = self.driver.execute_script(js, lab)

            item_name = lab.text.strip('：')
            # item = hwnd_item.find_element_by_css_selector('.el-form-item__content')
            # hwnd_ipts = item.find_elements_by_css_selector('input')

            js = "return arguments[0].querySelector('.el-form-item__content')"
            item = self.driver.execute_script(js, hwnd_item)
            js = "return arguments[0].querySelectorAll('input')"
            hwnd_ipts = self.driver.execute_script(js, item)
            for hwnd in hwnd_ipts:
                hwnd_type = hwnd.get_attribute('type')
                readonly_sign = hwnd.get_attribute('readonly')
                # 找到元素父级, 判断是否为时间输入控件
                js = r"return arguments[0].parentNode;"
                hwnd_father = self.driver.execute_script(js, hwnd)
                date_sign = True if 'date' in hwnd_father.get_attribute('class') else False

                if date_sign:
                    hwnd_type = 'slct_spec'
                elif hwnd_type == "text" and readonly_sign:
                    hwnd_type = 'slct'
                elem_func[item_name].append((hwnd_type, hwnd))

        if elem_func.__contains__(''):
            elem_func.pop('')
        return elem_func

    def send_elems_vals(self, paras, parentNode=None):
        '''
        循环传入的数据 ,获取数据类型对应的处理方法, 并执行
        :paras {'姓名':['慧慧'], '民族': ['汉族'], '多选':['2','4'], 单选:['1']}
        #单选 , 多选的val为勾选项的索引
        #text 和 select 如果传入 ['CLEAR'] 会清除输入框
        :return:
        '''

        time.sleep(0.2)
        elem_func = self.get_elems_ipts(parentNode)
        for key, vals in paras.items():
            # print('debug : ' + key)
            for idx, val in enumerate(vals):
                # 判断传入为空的值, 会跳过当前元素输入流程, 比如个人照片, 也可以手动传空跳过
                if not val:
                    continue
                # 判断传入的key不在页面时, 会跳过输入流程, 比如id
                if not elem_func.get(key, None):
                    continue
                for func_type, hwnd_item in elem_func.get(key)[idx].items():
                    time.sleep(0.1)
                    # 文件类型的元素跳过
                    if func_type == 'file':
                        continue
                    try:
                        js = 'arguments[0].scrollIntoView()'
                        self.driver.execute_script(js, hwnd_item)
                        if hwnd_item.get_attribute('disabled'):
                            continue
                        if not func_type in ['radio', 'checkbox']:
                            hwnd_item.click()
                    except Exception as e:
                        print('Send_element_val Err :', e)
                        continue
                    time.sleep(0.1)
                    self.iptfuncs.get(func_type)(hwnd_item, val)
                    # 时间输入控件选择后, 敲回车 ,清除下拉框
                    if func_type in ['slct_spec']:
                        time.sleep(0.1)
                        hwnd_item.send_keys(Keys.ENTER)

    def chose_advSearch(self, paras=None):
        '''
        高级搜索
        :param 为空时执行重置 , 有值时执行搜索
        ;eg {'性别': '女', '开始时间': '2019-10-15', '开始日期转正时间': '2019-10-15', '1年龄': '2', '2年龄': '3'}
        :return:
        '''

        # 判断参数是否为空, 如果为空则重置
        time.sleep(0.5)
        if not paras:
            icon_refresh = self.findElement(self.icon_refresh)
            js = r"return arguments[0].parentNode;"
            hwnd = self.driver.execute_script(js, icon_refresh)
            hwnd.click()
            return

        # 展开高级搜索功能
        try:
            hwnd = self.findElement(self.btn_downshape)
            hwnd.click()
        except Exception:
            pass

        # 获取高级搜索所有元素, 并生成字典
        noname_dict = {}
        search_dict = {}
        hwnd_ipts = self.findElements(self.ipts_searchs)
        for hwnd in hwnd_ipts:
            hwnd_temp = hwnd
            while True:
                hwnd_pn = self.findParentNode(hwnd_temp)
                if 'el-col' not in hwnd_pn.get_attribute('class'):
                    hwnd_temp = hwnd_pn
                else:
                    break
            hwnd_lab = self.findElement(('js', '.dateTitle'), hwnd_pn)
            lab_name = hwnd_lab.text if hwnd_lab else ''
            hwnd_name = hwnd.get_attribute('placeholder')
            if not hwnd_name:
                hwnd_name = str(noname_dict.setdefault(lab_name, 0) + 1)
                noname_dict[lab_name] = int(hwnd_name)
            select_sign = hwnd.get_attribute('readonly')
            if select_sign:
                search_dict[hwnd_name + lab_name] = ['slct', hwnd]
            else:
                search_dict[hwnd_name + lab_name] = ['text', hwnd]

        # 循环参数操作
        for key, val in paras.items():
            time.sleep(0.5)
            method, hwnd = search_dict.get(key)
            if method == 'text':
                hwnd.send_keys(val)
            elif method == 'slct':
                hwnd.click()
                time.sleep(0.5)
                self.chose_select(val)

        icon_search = self.findElement(self.icon_search)
        js = r"return arguments[0].parentNode;"
        hwnd = self.driver.execute_script(js, icon_search)
        hwnd.click()
        time.sleep(0.3)

    def get_detail_vals(self):
        '''
        获取详情页面字段详情
        :return: iptvals_info {'姓名': '慧慧', '入网年份': '2019', '性别': '女', '保险情况': '大病保险/扶贫保险/'}
        '''

        time.sleep(0.5)
        iptvals_info = defaultdict(str)
        peoname = self.findElement(self.lab_peoname)
        try:
            iptvals_info['姓名'] = peoname.text
        except Exception:
            self.detail.error('未获取到姓名')
        lab_iptvals = self.findElements(self.lab_iptvals)
        for lab in lab_iptvals:
            valname = lab.text.strip('：')
            js = r"return arguments[0].parentNode;"
            hwnd_farther = self.driver.execute_script(js, lab)
            hwnd_val = hwnd_farther.find_element_by_css_selector('.value')
            js = "return arguments[0].querySelector('[role=group]')"
            length = self.driver.execute_script(js, hwnd_val)
            if not length:
                iptvals_info[valname] = hwnd_val.text.strip()

            else:
                try:
                    checked_items = hwnd_val.find_elements_by_css_selector(
                        'label[class*=is-checked] .el-checkbox__label')
                    if checked_items:
                        for item in checked_items:
                            iptvals_info[valname] += item.text.strip() + '/'
                    else:
                        iptvals_info[valname] = None
                except:
                    iptvals_info[valname] = None
        try:
            iptvals_info.pop('')
        except KeyError:
            pass

        return iptvals_info

    def get_mustfill(self):
        '''
        获取所有必填项
        :return: ['姓名', '身份证号', '年龄']
        '''

        time.sleep(0.5)
        lab_mustfill = self.findElements(self.lab_mustfill)
        mustfill = [lab.text.strip('：') for lab in lab_mustfill]
        return mustfill

    def get_mustfillerr(self):
        '''
        获取必填项及必填项错误信息
        :return:
        ;errinfo {'姓名': '请输入姓名', '身份证号': '请填写身份证号', '性别': '请输入性别', '是否户主': '请输入是否户主'}
        '''

        time.sleep(0.5)
        errinfo = {}
        # lab_mustfills = self.findElements(self.lab_mustfill_err)
        js = f'return document.querySelectorAll("{self.lab_mustfill_err[1]}")'
        lab_mustfills = self.driver.execute_script(js)
        for lab in lab_mustfills:
            name = lab.text.strip('：')
            js = r"return arguments[0].parentNode;"
            hwnd_farther = self.driver.execute_script(js, lab)
            # erritems = hwnd_farther.find_elements_by_css_selector('.el-form-item__error')
            js = 'return arguments[0].querySelectorAll(".el-form-item__error")'
            erritems = self.driver.execute_script(js, hwnd_farther)
            err = ''
            for item in erritems:
                err += item.text
            errinfo[name] = err

        return errinfo

    def get_elem_style(self, element):
        '''
        获取元素的页面样式
        :return: {'color' : 'rgb(155,155,155)', 'font-size' : '14px'}
        '''

        time.sleep(0.5)
        element_info = {}
        style_temps = eval(self.config.get('style'))

        for temp in style_temps:
            style = element.value_of_css_property(temp)
            element_info[temp] = style

        return element_info

    def get_tabelements(self, needdata=True):
        '''
        获取列表数据详情
        :return: [{'姓名': ('慧慧', webelement), '年龄': ('18', webelement), '操作':('', webelemet)}, ....]
        webelement 是td>div 的元素定位, 可以直接通过text方法获取文本
        操作可以直接通过 webelemet 寻找下级的按钮
        '''

        time.sleep(0.3)
        tabelements = []

        hwnd_tabdiv = self.findElement(self.div_table)
        tab = self.findElement(('js', '[class*=is-scrolling-left]'), hwnd_tabdiv)
        if tab:
            for idx in range(1, 5):
                js = f'arguments[0].scrollLeft = {idx} * arguments[0].offsetWidth/2'
                self.driver.execute_script(js, tab)
                time.sleep(0.3)

        tabtitles = self.findElements(self.lab_tabtitles)
        titles = [data.get_attribute('innerHTML') for data in tabtitles]

        if not needdata:
            return titles

        tabrows = self.findElements(self.row_tabdatas)
        for tabrow in tabrows:
            tabtds = tabrow.find_elements_by_css_selector('td:not(.el-table-column--selection)>div span')
            tabdatas = [td.get_attribute('innerHTML') for td in tabtds]
            tabelements.append(dict(zip(titles, zip(tabdatas, tabtds))))

        return tabelements

    def clear_inputval(self, para):
        '''
        清空输入框
        :param para: self.get_elems_ipts 输出格式
        :return:
        '''

        time.sleep(0.5)
        for items in para.values():
            for val in items:
                for key, obj in val.items():
                    try:
                        if obj.get_attribute('value') and not obj.get_attribute('disabled'):
                            if key in ['slct', 'slct_spec']:
                                self.clear_inputval_byclose(obj)
                            else:
                                obj.send_keys(Keys.CONTROL + 'a')
                                obj.send_keys(Keys.DELETE)
                    except StaleElementReferenceException:
                        pass

    def clear_inputval_byclose(self, item):
        '''
        通过输入框的原型x按钮, 清除输入框内容
        :return:
        '''

        time.sleep(0.5)
        item.click()
        time.sleep(0.1)
        item.click()

        hwnds = self.findElements(self.btn_circleclose)
        for hwnd in hwnds:
            try:
                hwnd.click()
            except Exception as e:
                # self.error.error(e)
                continue

    def clear_inputval_byclear(self, item):
        '''
        用clear的方法, 清除输入框内容
        :return:
        '''
        time.sleep(0.5)
        item.send_keys(Keys.CONTROL + 'a')
        item.send_keys(Keys.DELETE)

    def click_picture_upload(self):
        '''
        获取新增页面上传图片的按钮
        :return:
        '''

        time.sleep(0.5)
        self.findElement(self.btn_picture).click()

    def get_picele(self):
        '''
        获取图片元素信息
        :return: {'个人照片':webelement}
        '''

        time.sleep(0.5)
        hwnd = self.findElement(self.div_picture)
        js = 'return arguments[0].querySelector("img")'
        rst = self.driver.execute_script(js, hwnd)
        if not rst:
            rst = '暂无照片'

        return {'个人照片': rst}

    def get_picele_markword(self):
        '''
        获取图片的提示语
        :return:
        '''
        time.sleep(0.5)
        hwnd = self.findElement(self.lab_markword)

        return hwnd

    def click_peo(self):
        '''
        获取新增页面选择人员的按钮
        :return:
        '''
        time.sleep(0.5)
        self.findElement(self.btn_peo).click()

    def chose_listpagefunc(self, method, para=None):
        '''
        列表翻页方法
        ;method: 总数, 前一页, 后一页, 页数, 跳转, 翻页
        ;para: 当method为 页数, 跳转, 翻页时需带相应参数 eg: 页数 10条, 跳转 3,
        翻页可用索引或文本
        翻页 int(2)
        翻页 str(2) , str(...)
        :return:
        '''

        time.sleep(0.5)
        div_page = self.findElement(self.div_listpage)

        # 各元素定位
        lab_totle = div_page.find_element_by_css_selector('.el-pagination__total')  # 列表条数
        btn_f, btn_b = div_page.find_elements_by_css_selector('button')  # 前翻页
        lis_page = div_page.find_elements_by_css_selector('ul li')  # 数字翻页
        slct_pagesize = div_page.find_element_by_css_selector('.el-pagination__sizes input')  # 每页数量
        ipt_page = div_page.find_element_by_css_selector('.el-pagination__jump input')  # 跳转页数

        func_switch = {}
        func_switch['总数'] = lambda x: re.search(r'(\d+)', lab_totle.text).group()
        func_switch['前一页'] = lambda x: btn_f.click()
        func_switch['后一页'] = lambda x: btn_b.click()
        func_switch['页数'] = lambda x: (slct_pagesize.click(), self.chose_select(x))
        func_switch['跳转'] = lambda x: (ipt_page.send_keys(x), ipt_page.send_keys(Keys.ENTER))

        def _lamb(x):
            if isinstance(x, int):
                hwnd = lis_page[x]
                hwnd.click()
            else:
                for li in lis_page:
                    if x in li.text:
                        x.click()

        func_switch['翻页'] = lambda x: _lamb(x)

        try:
            rst = func_switch.get(method)(para)
            return rst
        except Exception as e:
            raise ValueError(f'翻页方法错误 , {e}')

    def writecontent_iframe(self, content):
        '''
        在富文本编辑框写入文本
        :return:
        '''

        time.sleep(0.5)

        iframe = self.findElement(self.body_content)
        self.driver.switch_to.frame(iframe)

        hwnd = self.findElement((By.CSS_SELECTOR, 'p'))
        js = f"arguments[0].innerHTML = '{content}'"
        self.driver.execute_script(js, hwnd)

        self.driver.switch_to.default_content()

    def get_downloadcenter(self, sign=True):
        '''
        打开下载中心
        :return:
        '''

        time.sleep(0.3)
        if sign:
            hdl_icon = self.findElement(self.btn_downloadcenter)
            hdl_icon.click()
        else:
            hdl_icon = self.findElement(self.btn_downloadclose)
            hdl_icon.click()

    def chose_downloadcenterfunc(self, method, idx=0, para=None):
        '''
        下载中心操作
        : method: 下载, 获取密码
        : params: idx 下载数据索引位置
        :return:
        '''

        time.sleep(0.3)

        hdl_rows = self.findElements(self.row_downloaditems)
        row = hdl_rows[idx]

        lab_content = self.findElement('.fileName', row)
        btn_download = self.findElement('.iconDownload', row)
        btn_close = self.findElement(self.btn_downloadclose)

        func = {}
        func['下载'] = lambda: btn_download.click()
        func['获取密码'] = lambda: re.search(r'密码：(\d+)', lab_content.text).group(1)
        func['获取文件名'] = lambda: re.search('(.*?\.zip)', lab_content.text).group()
        func['关闭'] = lambda: btn_close.click()

        rst = func.get(method)()
        return rst
