# -*- coding:utf-8 -*-



import random
import time

from tools.name import *


def random_name():
    return ran_name()


def random_num(min=0, max=4):
    '''
    返回随机数字
    :return:
    '''

    if not min and not max:
        return ''

    num = random.randint(min, max)
    return num


def random_select(min=0, max=1):
    '''
    返回随机选择类型, (数字)
    :return:
    '''

    if not min and not max:
        return ''

    num = random.randint(min, max)
    return num


def random_cn(length=4):
    '''
    返回随机中文
    :param length:
    :return:
    '''
    if not (length):
        return ''

    # vars = [random.sample(Ming, 1) for _ in range(length)]
    vars = random.sample(Ming, length)
    return ''.join(vars)


def random_en(length=4):
    '''
    返回随机英文
    :param length:
    :return:
    '''
    if not (length):
        return ''

    vars = [chr(random.randint(65, 90)) for _ in range(length)]
    return ''.join(vars)


def random_float(min=0, max=4, round_l=2):
    '''
    返回随机浮点类型
    :param min:
    :param max:
    :return:
    '''
    if not min and not (max and round_l):
        return ''

    temp_num = random.uniform(min, max)
    num = round(temp_num, round_l)
    return num


def random_time(s_year=2000, l_year=2020, sign=True):
    '''
    返回随机时间
    :return:
    '''

    if not (s_year and l_year):
        return ''

    a1 = (int(s_year), 1, 1, 0, 0, 0, 0, 0, 0)  # 设置开始日期时间元组（1976-01-01 00：00：00）
    a2 = (int(l_year), 12, 31, 23, 59, 59, 0, 0, 0)  # 设置结束日期时间元组（1990-12-31 23：59：59）

    start = time.mktime(a1)  # 生成开始时间戳
    end = time.mktime(a2)  # 生成结束时间戳

    t = random.randint(start, end)  # 在开始和结束时间戳中随机取出一个
    date_touple = time.localtime(t)  # 将时间戳生成时间元组

    if sign:
        template = '%Y-%m-%d'
    else:
        template = '%Y-%m-%d %H:%M:%S'
    date = time.strftime(template, date_touple)  # 将时间元组转成格式化字符串（1976-05-21）

    return date


randata_switch = {}
randata_switch['num'] = random_num
randata_switch['cn'] = random_cn
randata_switch['time'] = random_time
randata_switch['float'] = random_float
