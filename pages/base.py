# -*- coding:utf-8 -*-
# import pyautogui as pyautogui
from selenium import webdriver
from selenium.common.exceptions import TimeoutException,NoSuchElementException
from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
# from util._common import *
from tools.logger import Logger
from selenium.common.exceptions import *

import time
import logging
import random
import traceback
from tools.common import Common


class Base(Common):
    """description of class"""
    """
    page基类，编写常用action，
    """
    logger = Logger()
    # webdriver instance
    def __init__(self, browser, sec=3):
        '''
        initialize selenium webdriver, use chrome as default webdriver
        '''
        super(Base, self).__init__()
        self.driver = browser  # 窗口句柄
        self.wait = WebDriverWait(browser, sec)  # 等待时间
        self.actions = ActionChains(browser)  # 鼠标句柄

    def findElement(self, element, parentnode=None):
        '''
        Find element

        element is a set with format (identifier type, value), e.g. ('id','username')

        Usage:
        self.findElement(element)
        '''

        try:
            if isinstance(element, tuple):
                type = element[0]
                value = element[1]
            else:
                type = 'css'
                value = element

            obj = parentnode if parentnode else self.driver

            if type == "id":
                elem = obj.find_element_by_id(value)

            elif type == "name" or type == "NAME" or type == "Name":
                elem = obj.find_element_by_name(value)

            elif type == "class" or type == "CLASS" or type == "Class":
                elem = obj.find_element_by_class_name(value)

            elif type == "link_text" or type == "LINK_TEXT" or type == "Link_text":
                elem = obj.find_element_by_link_text(value)

            elif type == "xpath" or type == "XPATH" or type == "Xpath":
                elem = obj.find_element_by_xpath(value)

            elif type == "css" or type == "CSS" or type == "css selector":
                elem = obj.find_element_by_css_selector(value)

            elif type == "js":
                if not parentnode:
                    js = f"return document.querySelector('{value}')"
                    elem = self.driver.execute_script(js)
                else:
                    js = f"return arguments[0].querySelector('{value}')"
                    elem = self.driver.execute_script(js, parentnode)
            else:
                raise NameError("Please correct the type in function parameter")

        except Exception:
            raise ValueError("No such element found" + str(element))
        return elem

    def findElements(self, element, parentnode=None):
        '''
        Find elements

        element is a set with format (identifier type, value), e.g. ('id','username')

        Usage:
        self.findElements(element)
        '''
        try:
            if isinstance(element, tuple):
                type = element[0]
                value = element[1]
            else:
                type = 'css'
                value = element

            obj = parentnode if parentnode else self.driver

            if type == "id" or type == "ID" or type == "Id":
                elem = obj.find_elements_by_id(value)

            elif type == "name" or type == "NAME" or type == "Name":
                elem = obj.find_elements_by_name(value)

            elif type == "class" or type == "CLASS" or type == "Class":
                elem = obj.find_elements_by_class_name(value)

            elif type == "link_text" or type == "LINK_TEXT" or type == "Link_text":
                elem = obj.find_elements_by_link_text(value)

            elif type == "xpath" or type == "XPATH" or type == "Xpath":
                elem = obj.find_elements_by_xpath(value)

            elif type == "css" or type == "CSS" or type == "css selector":
                elem = obj.find_elements_by_css_selector(value)

            elif type == "js":
                if not parentnode:
                    js = f"return document.querySelectorAll('{value}')"
                    elem = self.driver.execute_script(js)
                else:
                    js = f"return arguments[0].querySelectorAll('{value}')"
                    elem = self.driver.execute_script(js, parentnode)
            else:
                raise NameError("Please correct the type in function parameter")

        except Exception:
            raise ValueError("No such element found" + str(element))
        return elem

    def findParentNode(self, element):
        '''
        查找父元素
        :return:
        '''

        js = f"return arguments[0].parentNode;"
        elem = self.driver.execute_script(js, element)
        return elem

    def open(self, url=None):
        '''
        Open web url

        Usage:
        self.open(url)
        '''
        if url :
            self.driver.get(url)
        else:
            raise ValueError("please provide a base url")

    def type(self, element, text):
        '''
        Operation input box.

        Usage:
        self.type(element,text)
        '''
        element.send_keys(text)

    def enter(self, element):
        '''
        Keyboard: hit return

        Usage:
        self.enter(element)
        '''
        element.send_keys(Keys.RETURN)

    def click(self, element):
        '''
        Click page element, like button, image, link, etc.
        '''
        element.click()

    # def quit(self):
    #     '''
    #     Quit webdriver
    #     '''
    #     self.driver.quit()

    def getAttribute(self, element, attribute):
        '''
        Get element attribute

        '''
        return element.get_attribute(attribute)

    def getText(self, element):
        '''
        Get text of a web element

        '''
        return element.text

    def getTitle(self):
        '''
        Get window title
        '''
        return self.driver.title

    def getCurrentUrl(self):
        '''
        Get current url
        '''
        return self.driver.current_url

    def getScreenshot(self, targetpath):
        '''
        Get current screenshot and save it to target path
        '''
        self.driver.get_screenshot_as_file(targetpath)

    def maximizeWindow(self):
        '''
        Maximize current browser window
        '''
        self.driver.maximize_window()

    def back(self):
        '''
        Goes one step backward in the browser history.
        '''
        self.driver.back()

    def forward(self):
        """
        Goes one step forward in the browser history.
        """
        self.driver.forward()

    def getWindowSize(self):
        """
        Gets the width and height of the current window.
        """
        return self.driver.get_window_size()

    def refresh(self):
        '''
        Refresh current page
        '''
        self.driver.refresh()
        self.driver.switch_to()

    def wait(self,ele):
        '''
        30秒内hwnd为空，返回false
        :return:
        '''
        step = 0
        while True:
            step += 1
            js = f'return document.querySelector({ele})'
            hwnd = self.driver.execute_script(js)
            if hwnd:
                return hwnd
            elif step == 30:
                self.logger.erro(f"寻找{ele}元素时间超时")
                self.get_screenShot()
                return False


    def find_element_and_wait(self,locator):
        """
        :param locator: 元组形式(By.ID,"id")
        :return: 返回element
        """
        element = None
        try:
            # element = WebDriverWait(self.driver,30).until(ec.presence_of_element_located(*locator))
            element = WebDriverWait(self.driver,5,0.1).until(lambda driver:self.driver.find_element(*locator))
        except TimeoutException as e:
            #后期改为logger
            self.logger.erro(f"寻找{locator}元素时间超时")
            self.get_screenShot()
            # print("寻找元素时间超时")
        return element

    def find_elements_and_wait(self,locator):
        """
        :param locator: 元组形式(By.ID,"id")
        :return: 返回element
        """
        elements = None
        try:
            # element = WebDriverWait(self.driver,30).until(ec.presence_of_element_located(*locator))
            elements = WebDriverWait(self.driver,2,0.5).until(lambda driver:self.driver.find_elements(*locator))
        except TimeoutException as e:
            #后期改为logger
            self.logger.erro(f"寻找{locator}元素时间超时")
            self.get_screenShot()
            # print("寻找元素时间超时")
        return elements

    def open_home(self,url):
        # self.logger.info(f"打开{url}")
        self.driver.get(url)


    def click_element(self,locator):
        '''
        :param locator: 元组形式(By.ID,"id")
        :return:
        '''
        # self.logger.info(f"点击{locator}元素")
        js = f'return document.querySelector("{locator[-1]}")'
        element = self.driver.execute_script(js)
        # element = self.find_element_and_wait(locator)
        if element:
            element.click()
            time.sleep(0.3)
        else:
            return False

    def jsclick(self,locator):
        """
        用js的方法做点击
        :param locator:
        :return:
        """
        js = f'return document.querySelector("{locator[-1]}")'
        element = self.driver.execute_script(js)
        if element:
            self.driver.execute_script("arguments[0].click()",element)
            time.sleep(0.3)
        else:
            return False


    def send_text(self,locator,text):
        '''
        :param locator: 元组形式(By.ID,"id")
        :param text: send_keys输入值
        :return:
        '''
        # self.logger.info(f"在{locator}上输入{text}")
        element = self.find_element_and_wait(locator)
        if element:
            element.clear()
            element.send_keys(text)
            time.sleep(0.2)
        else:
            return False

    def execute_js_click(self,locator):
        '''
        :param locator: 元组形式(By.ID,"id")
        :return:
        '''
        # self.logger.info(f"使用js点击{locator}")
        element = self.find_element_and_wait(locator)
        if element:
            self.driver.execute_script("arguments[0].click();",element)
            time.sleep(0.2)
        else:
            return False

    def is_exist(self,locator):
        '''
        :param locator: 元组形式(By.ID,"id")
        :return:
        '''
        flag = True
        try:
            self.driver.find_element(*locator)
        except NoSuchElementException:
            flag = False
        return flag

    def delete_cookies(self):
        # self.logger.info("清除cookies")
        self.driver.delete_all_cookies()

    def get_screenShot(self):
        # self.logger.info("截图")
        self.save_screen_shot(self.driver)

    def switch_frame(self,locator):
        '''
        :param locator: 字符串，并且要么是唯一的id，要么是唯一的name
        :return:
        '''
        # self.logger.info("进入iframe")
        self.driver.switch_to.frame(locator)


    def switch_to_default(self):
        # self.logger.info("退出iframe")
        self.driver.switch_to.default_content()

    def quit(self):
        # self.logger.info("关闭浏览器")
        self.driver.quit()


