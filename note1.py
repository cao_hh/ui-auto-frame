# -*- coding:utf-8 -*-
"""
@Time:2021/3/22 17:36
@Auth: Chh
@Function:请输入....
"""
# This sample code uses the Appium python client
# pip install Appium-Python-Client
# Then you can paste this into a file and simply run with Python

# This sample code uses the Appium python client
# pip install Appium-Python-Client
# Then you can paste this into a file and simply run with Python

from appium import webdriver

caps = {}
caps["platformName"] = "Android"
caps["platformVersion"] = "6.0.1"
caps["deviceName"] = "127.0.0.1:7555"
caps["appPackage"] = "com.tencent.mobileqq"
caps["appActivity"] = "com.tencent.mobileqq.activity.SplashActivity"
caps["noReset"] = True
caps["unicodeKeyboard"] = True
caps["resetKeyboard"] = True
caps["automationName"] = "uiautomator1"
caps["ensureWebviewsHavePages"] = True

driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", caps)
driver.implicitly_wait(10)
el1 = driver.find_element_by_accessibility_id("请输入QQ号码或手机或邮箱")
el1.clear()
el1.send_keys("2576726181")
el2 = driver.find_element_by_accessibility_id("密码 安全")
el2.clear()
el2.send_keys("806117chh")
el3 = driver.find_element_by_accessibility_id("登 录")
el3.click()
el4 = driver.find_element_by_accessibility_id("帐户及设置")
el4.click()
el5 = driver.find_element_by_id("com.tencent.mobileqq:id/gso")
el5.click()
el6 = driver.find_element_by_id("com.tencent.mobileqq:id/account_switch")
el6.click()
el7 = driver.find_element_by_accessibility_id("退出当前帐号按钮")
el7.click()
el8 = driver.find_element_by_id("com.tencent.mobileqq:id/dialogRightBtn")
el8.click()

driver.quit()