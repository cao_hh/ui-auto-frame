# -*- coding:utf-8 -*-
"""
 @Time:2021/4/16 12:49
 @Auth: Chh
 @Function:请输入....
"""

'''
blocker级别：中断缺陷（客户端程序无响应，无法执行下一步操作）
critical级别：临界缺陷（ 功能点缺失）
normal级别：普通缺陷（数值计算错误）
minor级别：次要缺陷（界面错误与UI需求不符）
trivial级别：轻微缺陷（必输项无提示，或者提示不规范）
'''

from tools.assert_testcase import NewTestcase
from tools.getdata_csv import *
import pytest
from tools.getdata_xlsx import *
# from tools import logger

@allure.feature('党建党务-三会一课')
class TestLogin(NewTestcase):
    wy = None  # 可以当做全局变量使用，那么在使用之前就需要用global，如果当做类变量，那么直接用TestLogin.wy调用该变量
    r = Rc()

    @allure.story('新增')
    @allure.title('“登录”字段验证')
    def test_001_wyy(self, function):
        '''
        todo:登录字段校验
        :return:  function就是conftest文件function的返回值driver
                这里的入参，必须是conftest的函数名，也就是function

        '''
        TestLogin.wy = Wyy(function)
        hwnd = self.wy.wyy_text_login() #这里的self.wy就是TestLogin.wy，都是调用的类变量wy

        self.assertEqual_new(hwnd.text, "登录", "登录字段显示异常", function)

    @allure.story('登录用例')
    @pytest.mark.parametrize("param", r.pd_excel('test_002_wyy'))
    def test_002_wyy(self, param, function):
        '''
        登录跳转验证  function：是conftest文件的function函数名，也可以直接用函数名当返回值driver使用
        :param title:
        :param username:
        :param passwd:
        :param excepted:
        :param function:
        :return:
        '''
        username = eval(param.get('请求参数')).get('username')
        password = eval(param.get('请求参数')).get('password')
        with allure.step(f'1.打开浏览器2.点击登录按钮3.输入用户名{username}4.输入密码{password}'):

            '''直接使用全局变量wy'''
            hwnd,sign = TestLogin.wy.wyy_login(username,password)

        allure.dynamic.title(param.get('标题'))

        if not sign: # false+not为true ，如果sign为真
            self.assertTrue_new(hwnd, "错误输入，登录弹框消失", function)
        else:
            self.assertEqual_new(hwnd,'我的主页','登录成功',function)
