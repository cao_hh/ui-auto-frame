#!/usr/bin/env python
# -*- coding:utf-8 -*-
# ====#====#====#====
# Author:
# CreatDate:
# Version:
'''
blocker级别：中断缺陷（客户端程序无响应，无法执行下一步操作）
critical级别：临界缺陷（ 功能点缺失）
normal级别：普通缺陷（数值计算错误）
minor级别：次要缺陷（界面错误与UI需求不符）
trivial级别：轻微缺陷（必输项无提示，或者提示不规范）
'''
# ====#====#====#====
from tools.assert_testcase import NewTestcase
from tools.getdata_csv import *
from pages.baidu.baidu_page import *
import pytest

@allure.feature('党建党务-三会一课')
class TestBaidu(NewTestcase):

            @pytest.mark.parametrize('filed',['python'])
            @allure.title('百度搜索')
            def test_001_baidu(self,function,filed):

                bd = Baidu(function)
                hwnd = bd.bd_text(filed)

                self.assertTrue_new(hwnd,function)


