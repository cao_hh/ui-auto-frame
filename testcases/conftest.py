# -*- coding:utf-8 -*-
'''auhor: chh'''

import pytest
from webDriver.driver_factory import DriverFactory
from pages.baidu.baidu_page import *
from  selenium.webdriver import Chrome,ChromeOptions
'''
经典初始化不支持多个py初始化，只能选用fixture;
　　scope值 ：默认是function；

　　session: 多个py文件；

　　module: 一个py文件里面多个class或function；

　　class： class级别；一个类开始跑一次，结束跑一次，相当于setupclass和teardownclass

　　function：函数级别；每个函数开始跑一次，结束跑一次，相当于setup和teardown

由于我们是多个py所以，只能选择session；
yield 是setup和teardown分界线

autouse = True   fixture里面有个参数autouse，默认是Fasle没开启的，需要显示就调用；
可以设置为True开启自动使用fixture功能，这样用例就不用每次都去传参了；
'''
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

driver = None

@pytest.fixture(scope="session", autouse=True)
def start_up_session():
    '''初始化工具类'''

    global driver

    """自动化无界面运行"""
    opt = Options()
    opt.add_argument('--no-sandbox')  # 解决DevToolsActivePort文件不存在的报错
    # opt.add_argument('window-size=1920x3000')  # 设置浏览器分辨率
    opt.add_argument('--disable-gpu')  # 谷歌文档提到需要加上这个属性来规避bug
    # opt.add_argument('--hide-scrollbars')  # 隐藏滚动条，应对一些特殊页面
    # opt.add_argument('blink-settings=imagesEnabled=false')  # 不加载图片，提升运行速度
    # opt.add_argument('--headless')  # 浏览器不提供可视化界面。Linux下如果系统不支持可视化不加这条会启动失败

    '''初始化工具类'''
    driver = Chrome(options=opt)
    # driver = DriverFactory.get_driver()
    driver.maximize_window()
    driver.implicitly_wait(5)

    # yield 前面的是setupclass的操作
    yield
    # yield 后面的是teardownclass的操作
    driver.quit()

@pytest.fixture(autouse=True)
def function():

    '''初始化工具类'''

    co = Common()
    basePage = Base(driver)

    '''打开浏览器'''
    basePage.open_home(co.config.get('host'))

    # yield 前面的是setup的操作
    yield driver
    # yield 后面的是teardown的操作


'''如果有多个url时，可以使用下面的方法，结合测试用例装饰器pytest.mark.parametrize一起使用
具体可参考https://www.cnblogs.com/jodie2019/p/13200291.html
'''
# @pytest.fixture(scope="function",autouse=True)
# def start_up_function(request):
#     co = Common()
#     basePage = Base(driver)
#
#     '''打开浏览器'''
#     if request.param == 'True':
#         basePage.open_home(co.config.get('host'))
#     elif request.param == 'None':
#         basePage.open_home(co.config.get('hostbaidu'))
#
#     # yield 前面的是setup的操作
#     yield driver
#     # yield 后面的是teardown的操作
