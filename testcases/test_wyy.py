# -*- coding:utf-8 -*-
"""
 @Time:2021/4/16 13:22
 @Auth: Chh
 @Function:请输入....
"""
'''
blocker级别：中断缺陷（客户端程序无响应，无法执行下一步操作）
critical级别：临界缺陷（ 功能点缺失）
normal级别：普通缺陷（数值计算错误）
minor级别：次要缺陷（界面错误与UI需求不符）
trivial级别：轻微缺陷（必输项无提示，或者提示不规范）
'''

from tools.assert_testcase import NewTestcase
from tools.getdata_csv import *

@allure.feature('网易云音乐-搜索框')
class TestWyySearch(NewTestcase):

            @allure.title('首页搜索框')
            def test_004_wyy(self,function):

                wy = Wyy(function)
                hwnd = wy.wyy_search("绿光")

                self.assertEqual_new(hwnd,"绿光", f"{hwnd}和绿光不匹配",function)


